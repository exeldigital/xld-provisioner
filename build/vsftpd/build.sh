#!/bin/bash

echo -e "\n=== Installing VSFTP ===\n"
sudo apt-get install -y postfix

echo -e "\n=== Copying VSFTP config files ===\n"
local_path=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)
sudo cp -Rv $local_path/etc /

echo -e "\n=== Configuring Firewall ===\n"
sudo ufw allow Ftp

echo -e "\n=== Restarting VSFTPD ===\n"
sudo service vsftpd restart
