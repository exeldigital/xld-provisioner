#!/bin/bash
# Update certbot ssl certificates weekly.

/usr/local/sbin/certbot-auto renew >> /var/log/le-renew.log
