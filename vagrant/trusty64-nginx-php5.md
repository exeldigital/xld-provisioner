## Trusty64-Nginx-PHP5

Originally built from the Vagrant community base box "ubuntu/trusty64", this box
includes the following pre-installed and pre-configured services, tuned for
running Drupal, Magento, and Wordpress sites.

- Ubuntu 14 (Trusty64)
- Nginx
- PHP 5.6 FPM with extensions:
    - cli, common, curl, gd, json, mbstring, mcrypt, mysql, opcache, readline,
      soap, xml, zip
- MySQL 5.6
- Adminer
- Mailhog
- php-xdebug
    - Disabled by default. Enable with: `sudo phpenmod xdebug`
- Drush 8 and Registry Rebuild
- NodeJS
- Composer
- SASS
- Compass
- Behat
- n98-magerun
- wp-cli
- Platform CLI

### Usage and notes
- Nginx is configured for automatic virtual hosts.
    - Project site docroot(s) should be located in: `/var/www/example.dev/web`
- Database root user / password:
    - root / root
- PHPInfo page is available on port 8000:
    - <http://vagrant.dev:8000>
- Adminer is available on port 8181:
    - <http://vagrant.dev:8181>
- MailHog is available on port 8025:
    - <http://vagrant.dev:8025>
- Additional custom service settings as found in the `/build` directory.

### Release Notes

#### v1.4.0
- Removed all symlink files for Windows filesystem compatibility.
- Added NodeJS.
- Upgraded MySQL Server from 5.5 to 5.6.
- Increased the php upload max file size.
- Routine updates.

#### v1.3.0
- Adding Drush shell aliases file.

#### v1.2.0
- Removed ssh passphrase caching, in favor of ssh agent forwarding.

#### v1.1.2
- Routine updates.

#### v1.1.1
- Enable `vagrant-webserver.ini` and `mailhog.ini` configs for php-fpm.
- Adding new php config file: `vagrant-fpm.ini`

#### v1.1.0
- Addressing broken nginx config.
    - Adjusted php-fpm socket name in `www.conf`.
    - Refereing php-fpm socket correctly in site `.conf` file.
    - Adding server_name directive to nginx site `.conf` files.
    - Configuring php-fpm to run as user:group vagrant:vagrant in pool.d file
      `www.conf`.

#### v1.0.0
- Forked from vagrant build scripts for box [`trusty64-apache2-php5`] `v4.0.2`

### Intended _Trusty64-Nginx-PHP5_ Box Maintenance Workflow

1. Clone this project to a directory on your local machine and `cd` into the
   project:

        git clone git@bitbucket.org:exeldigital/xld-provisioner.git
        cd xld-provisioner

2. Create a basic Vagrantfile.

        cp Vagrantfile.trusty64 Vagrantfile

3. Update the `trusty64` base box.

        vagrant box update

4. Boot the box.

        vagrant up

5. SSH into the box with `vagrant ssh`.

        vagrant ssh

6. Run the `build.sh` script to automatically install packages, apply
   Vagrant-ooptimized configurations, and clean up.

        /vagrant/build/vagrant-build.sh nginx php5

7. Clear the Bash history and exit.

        cat /dev/null > ~/.bash_history && history -c && exit

8. Re-package the VM.

        vagrant package --output trusty64-nginx-php5.box

9. Note the updated version and changes under the _Release Notes_ section of
    this `trusty64-nginx-php5.md` file.

10. Browse to the [Atlas project page][], log in, and select _New version_ from
    the left-hand menu.

11. Increment the version, note any changes in the _Description_ field, and
    click _Create version_.

12. Click _Create new provider_, set the _Provider_ field to `virtualbox`, and
    upload the newly-created box file.

13. When the upload has completed, click _Finish_, select _Versions_ from the
    left-hand menu, click the _unreleased_ link to edit the newly-created box
    version, and then click _Release version_ to make the version public.

14. Sync the updated Release Notes to [BitBucket][].

[Atlas project page]: https://app.vagrantup.com/exeldigital/boxes/trusty64-nginx-php5
[BitBucket]: https://bitbucket.org/exeldigital/xld-provisioner
