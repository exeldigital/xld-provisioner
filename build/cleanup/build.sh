#!/bin/bash

echo -e "\n=== Running apt-get autoremove ===\n"
sudo apt-get -y autoremove

echo -e "\n=== Running apt-get clean ===\n"
sudo apt-get clean

echo -e "\n=== Clearing cache directories ===\n"
sudo rm -rfv `find ~/ -type d -name '.cache'`
sudo rm -rfv `find ~/ -type d -name 'cache'`
sudo rm -rfv `find /usr/local/src -type d -name 'cache'`
sudo find /var/cache -type f ! -name '.*' -exec rm -rfv {} \;

echo -e "\n=== Clearing system logs ===\n"
sudo find /var/log -type f ! -name '.*' -exec rm -rfv {} \;

echo -e "\n=== Cleaning out the home directory ===\n"
sudo rm -v ~/.lesshst ~/.nano_history ~/.mysql_history
sudo rm -rfv ~/.config ~/.local ~/.platformsh ~/.terminus ~/terminus

echo -e "\n=== Confirming ownership of home directory files ===\n"
sudo chown -Rv $USER:$(id -g -n $USER) $HOME

echo -e "\n=== Zeroing out the drive ===\n"
sudo dd if=/dev/zero of=/EMPTY bs=1M
sudo rm -fv /EMPTY
