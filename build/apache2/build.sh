#!/bin/bash

echo -e "\n=== Installing Apache2 ===\n"
sudo apt-get -y install apache2
sudo a2enmod rewrite ssl vhost_alias

echo -e "\n=== Removing the default site ===\n"
sudo a2dissite 000-default
sudo rm -rv /var/www/html
