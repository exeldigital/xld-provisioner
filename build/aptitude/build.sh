#!/bin/bash

echo -e "\n=== Adding extra repositories ===\n"
sudo add-apt-repository -y ppa:ondrej/php

echo -e "\n=== Running apt-get update ===\n"
sudo apt-get update

echo -e "\n=== Running apt-get upgrade ===\n"
sudo apt-get -y upgrade

echo -e "\n=== Running apt-get dist-upgrade ===\n"
sudo apt-get -y dist-upgrade

echo -e "\n=== Installing basic packages ===\n"
sudo apt-get -y install git \
                        htop \
                        ruby-compass
