## Trusty64-Apache2-PHP5

Originally built from the Vagrant community base box "ubuntu/trusty64", this box
includes the following pre-installed and pre-configured services, tuned for
running Drupal, Magento, and Wordpress sites.

- Ubuntu 14 (Trusty64)
- Apache 2.4
- PHP 5.6 with extensions:
    - cli, common, curl, gd, json, mbstring, mcrypt, mysql, opcache, readline,
      soap, xml, zip
- MySQL 5.6
- Adminer
- Mailhog
- php-uploadprogress
- php-xdebug
    - Disabled by default. Enable with: `sudo phpenmod xdebug`
- Drush 8 and Registry Rebuild
- NodeJS
- Composer
- SASS
- Compass
- Behat
- n98-magerun
- wp-cli
- Platform CLI

### Usage and notes
- Apache2 is configured for automatic virtual hosts.
    - Project site docroot(s) should be located in: `/var/www/example.dev/web`
- Database root user / password:
    - root / root
- PHPInfo page is available on port 8000:
    - <http://vagrant.dev:8000>
- Adminer is available on port 8181:
    - <http://vagrant.dev:8181>
- MailHog is available on port 8025:
    - <http://vagrant.dev:8025>
- Additional custom service settings as found in the `/build` directory.

### Release Notes

#### v4.4.0 (in progress)
- Added SSL support
- Routing updates.

#### v4.3.0
- Removed all symlink files for Windows filesystem compatibility.
- Added NodeJS.
- Upgraded MySQL Server from 5.5 to 5.6.
- Increased the php upload max file size.
- Routine updates.

#### v4.2.0
- Adding Drush shell aliases file.

#### v4.1.0
- Removed ssh passphrase caching, in favor of ssh agent forwarding.

#### v4.0.3
- Routine updates.

#### v4.0.2
- Addressing broken hostbox-based mysql cli functionality.
    - Fixed file name typo in directory `/etc/mysql/conf.d` (changed
      `vagrant.conf` to `vagrant.cnf`).
    - Added `IDENTIFIED BY '$dbpass'` to privilege grant in the vagrant build
      script.

#### v4.0.1
- Making the Apache2 service run as user/group vagrant/vagrant, with file
  `/etc/apache2/conf-enabled/vagrant.conf`
- Adding some default bash aliases and ssh passphrase caching, with file
  `/etc/bash_completion.d/xld_bash`.
- Adding some default nano settings, with file `/etc/nanorc`.

#### v4.0.0
- Re-factored the provisioning process to use sub-scripts.
- Enabled Apache2 mod: `vhost_alias`
- Set up mass virtual hosts for `/var/www`
- Hosting the PHP Info page on port 8000.
- Hosting Adminer on port 8181.
- Changing ownership of file `~/.my.cnf`
- Routine updates

#### v3.0.3
- Enabled Apache2 mod: `rewrite`
- Added php extension: `soap`
- Routine updates

#### v3.0.2
- Allow mysql cli access from host machine.

#### v3.0.1
- Added MySQL multi-byte UTF-8 support, per
  <https://www.drupal.org/node/2754539>
- Added back Drush Registry Rebuild

#### v3.0.0
- Introduced installation script `vagrant-xld-install.sh`.
- Re-structured the files in this repo.
- Re-built the Vagrant box from base box `ubuntu/trusty64` using the
  newly-created installation script.

#### v2.2.0
- Created config directories in this project to track settings outside of the
  Vagrant box:
    - `/build/trusty64-apache2-php5/etc`
    - `/build/trusty64-apache2-php5/home`
    - `/build/trusty64/apache2-php5/var`
- Removed all `php5-*` packages.
- Removed phpmyadmin.
- Installed Adminer.
- Cleaned up PHP 5.5 config files.
- Removed directory: `/build`

#### v2.1.1
- Installed new packages: `php5.6-gd`, `php5.6-mcrypt`

#### v2.1.0

- Removed unwanted network setting from `/etc/network/interfaces`
- Added new apt repo: `ppa:ondrej/php`.
- Upgraded PHP 5.5 to 5.6 per [this Stack Overflow thread][1].
- Disabled Apache2 mod `php5` & enabled mod: `php5.6`
- Installed new packages: `php5.6-cli`, `php5.6-curl`, `php5.6-mbstring`,
  `php5.6-mysql`, `php-uploadprogress`, `php5.6-xml`, `php5.6-xdebug`,
  `php5.6-zip`
- Cleaned up the `~/` directory
- Routine updates

[`trusty64-apache2-php5`]: trusty64-apache2-php5.md
[1]: http://stackoverflow.com/questions/40567133/cannot-add-ppa-ppaondrej-php5-5-6

#### v2.0.0
- Moved box hosting from private server to Hashicorp Atlas.

#### v1.3.6
- Routine updates

#### v1.3.5
- Added a welcome message (`/etc/motd`)
- Routine updates

#### v1.3.4

- Fixed MailHog `smtp-addr` line in php ini file
- Removed the default drupal/magento/wordpress databases

#### v1.3.3

- Configured phpMyAdmin to run on port 8080
- Configured phpMyAdmin to automatically login
- Configured MailHog to run on port 8025 at startup
- Updated services & packages

#### v1.3.2

- Added file `~/.my.cnf`
- Setting PHP memory limit to `-1`
- Disabling PHP Xdebug
- Updated service & packages

#### v1.3.1

- Updated all mysql users to have SUPER priviliges.
- Updated services & packages to latest versions.

#### v1.3.0

- Moved Apache2 & MySQL logs back to original location
- Added Platform CLI
- Introduced & ran automatic update script: `vagrant-xld-update.sh`

#### v1.2.0

- Moved MySQL custom config to a new file: `/etc/mysql/conf.d/vagrant-xld.cnf`
- Moved PHP custom config to a new file: `/etc/php5/mods-available/vagrant-xld.ini`

#### v1.1.0

- Removed empty directory `~/terminus`
- Opened up MySQL to access from the host machine by changing bind-address to
  `0.0.0.0` in `/etc/my.cnf`

### Intended _Trusty64-Apache2-PHP5_ Box Maintenance Workflow

1. Clone this project to a directory on your local machine and `cd` into the
   project:

        git clone git@bitbucket.org:exeldigital/xld-provisioner.git
        cd xld-provisioner

2. Create a basic Vagrantfile.

        cp Vagrantfile.trusty64 Vagrantfile

3. Update the `trusty64` base box.

        vagrant box update

4. Boot the box.

        vagrant up

5. SSH into the box with `vagrant ssh`.

        vagrant ssh

6. Run the `build.sh` script to automatically install packages, apply
   Vagrant-ooptimized configurations, and clean up.

        /vagrant/build/vagrant-build.sh apache2 php5

7. Clear the Bash history and exit.

        cat /dev/null > ~/.bash_history && history -c && exit

8. Re-package the VM.

        vagrant package --output trusty64-apache2-php5.box

9. Note the updated version and changes under the _Release Notes_ section of
    this `trusty64-apache2-php5.md` file.

10. Browse to the [Atlas project page][], log in, and select _New version_ from
    the left-hand menu.

11. Increment the version, note any changes in the _Description_ field, and
    click _Create version_.

12. Click _Create new provider_, set the _Provider_ field to `virtualbox`, and
    upload the newly-created box file.

13. When the upload has completed, click _Finish_, select _Versions_ from the
    left-hand menu, click the _unreleased_ link to edit the newly-created box
    version, and then click _Release version_ to make the version public.

14. Sync the updated Release Notes to [BitBucket][].

[Atlas project page]: https://app.vagrantup.com/exeldigital/boxes/trusty64-apache2-php5
[BitBucket]: https://bitbucket.org/exeldigital/xld-provisioner
