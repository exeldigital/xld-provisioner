#!/bin/bash

echo -e "\n=== Opening up permissions on /usr/local/src ===\n"
sudo chown -v $USER:$(id -g -n $USER) /usr/local/src

echo -e "\n=== Installing NodeJS 6 ===\n"
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt -y install nodejs

echo -e "\n=== Installing Composer ===\n"
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
sudo rm -rf ~/.composer

echo -e "\n=== Installing Behat & Drupal Extension ===\n"
mkdir -v /usr/local/src/behat && cd /usr/local/src/behat
composer require drupal/drupal-extension
sudo ln -sv /usr/local/src/behat/vendor/bin/behat /usr/local/bin/

echo -e "\n=== Installing Coder ===\n"
mkdir -v /usr/local/src/coder && cd /usr/local/src/coder
composer require drupal/coder
sudo ln -sv /usr/local/src/coder/vendor/bin/phpcs /usr/local/bin/

echo -e "\n=== Installing Drush & Registry Rebuild ===\n"
mkdir -v /usr/local/src/drush && cd /usr/local/src/drush
composer require drush/drush
sudo ln -sv /usr/local/src/drush/vendor/bin/drush /usr/local/bin/
drush @none dl registry_rebuild-7.x

echo -e "\n=== Installing n98-magerun ===\n"
mkdir -v /usr/local/src/n98-magerun && cd /usr/local/src/n98-magerun
wget https://files.magerun.net/n98-magerun.phar -O n98-magerun
chmod 775 n98-magerun
sudo ln -sv /usr/local/src/n98-magerun/n98-magerun /usr/local/bin/

echo -e "\n=== Installing Platform.sh CLI ===\n"
curl -sS https://platform.sh/cli/installer | php
cp -v /etc/skel/.bashrc ~/
mv -v ~/.platformsh /usr/local/src/platform
sudo ln -sv /usr/local/src/platform/bin/platform /usr/local/bin/

echo -e "\n=== Installing Terminus ===\n"
mkdir -v /usr/local/src/terminus && cd /usr/local/src/terminus
composer require pantheon-systems/terminus
sudo ln -sv /usr/local/src/terminus/vendor/bin/terminus /usr/local/bin/

echo -e "\n=== Installing WP-CLI ===\n"
mkdir -v /usr/local/src/wp-cli && cd /usr/local/src/wp-cli
composer require wp-cli/wp-cli
sudo ln -sv /usr/local/src/wp-cli/vendor/bin/wp /usr/local/bin/

echo -e "\n=== Copying development tool config files ===\n"
local_path=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)
sudo cp -Rv $local_path/etc /

echo -e "\n=== Configuring home directory ===\n"
mkdir -pv ~/.nano
