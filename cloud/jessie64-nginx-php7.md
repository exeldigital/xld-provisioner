## Jessie64-Nginx-PHP7

This collection of provisioning scripts is to assist developers with quickly
standing up a production-ready Debian 8 (Jessie64) based web server, tuned for
Drupal, Magento, and Wordpress sites. This project installs and configures the
following packages on a newly-provisioned server:

- Nginx
- PHP 7.1 FPM with extensions:
    - cli, common, curl, gd, json, mbstring, mcrypt, mysql, opcache, readline,
      soap, xml, zip
- MySQL 14
- Adminer
- Mailhog
- Jenkins CI
- Mattermost
- Drush 8 and Registry Rebuild
- Composer
- SASS
- Compass
- Behat
- n98-magerun
- wp-cli
- Platform CLI

### Usage and notes
- Nginx is configured for automatic virtual hosts.
    - Project site docroot should be located in: `var/www/example.dev/web`
- PHPInfo page is available on port 8000:
    - <http://vagrant.dev:8000>
- Adminer is available on port 8181:
    - <http://vagrant.dev:8181>
- MailHog is available on port 8025:
    - <http://vagrant.dev:8025>
- Additional custom service settings as found in the `/build` directory.

### Release Notes

#### v1.0.0 (in progress)
- Initial release.

### ToDo

- Test this out on AWS and on DigitalOcean

### Intended _Jessie64-Nginx-PHP7_ Provisioning Workflow

1. Provision a new Jessie64-based server on AWS or DigitalOcean.

2. SSH into the newly-created server as a non-root user.

3. Give your user administrative privileges.

        visudo

4. Install git.

        sudo apt-get install git

5. Clone this project to your home directory.

        cd ~
        git clone git@bitbucket.org:exeldigital/vagrant-boxes.git provisioner
        cd provisioner

2. Create a basic Vagrantfile.

        cp Vagrantfile.trusty64 Vagrantfile

6. Run the `could-build.sh` script to install packages and for automatic
   cleanup.

        ~/provisioner/build/cloud-build.sh nginx php7

7. Clear the Bash history and exit.

        cat /dev/null > ~/.bash_history && history -c && exit

9. Note the updated version and changes under the _Release Notes_ section of
    this `trusty64-apache2-php7.md` file.

10. Sync the updated Release Notes to [BitBucket][].

[BitBucket]: https://bitbucket.org/exeldigital/vagrant-boxes
