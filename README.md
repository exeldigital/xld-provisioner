# Exel Digital Debian & Ubuntu Environment Provisioner

This project is a collection of provisioning scripts for cloud servers and
Vagrant boxes created and maintained by Exel Digital.

All pre-provisioned Vagrant boxes and servers are tuned for running Drupal,
Magento, and Wordpress sites.

The following services and tools come pre-installed:

- Apache2/Nginx
- PHP/PHP-FPM
- MySQL 14
- Adminer
- Drush 8 and Drush Registry Rebuild
- Composer
- SASS
- Compass
- Behat
- n98-magerun
- wp-cli
- Platform CLI

## Summary of Vagrant Box Projects

The following Vagrant boxes are available for download at
<https://atlas.hashicorp.com/exeldigital>:

- [`trusty64-apache2-php5`](vagrant/trusty64-apache2-php5.md)
- [`trusty64-apache2-php7`](vagrant/trusty64-apache2-php7.md)
- [`trusty64-nginx-php5`](vagrant/trusty64-apache2-php5.md)
- [`trusty64-nginx-php7`](vagrant/trusty64-apache2-php5.md)

### Initializing an Exel Digital Vagrant box

To create a basic `Vagrantfile` for the `trusty64-nginx-php7` box:

```ruby
vagrant init -m exeldigital/trusty64-nginx-php7
```

To add basic local network hosting to the box, edit `/Vagrantfile` and add the
following:

```ruby
  # Configure networking.
  config.vm.network :private_network, ip: "192.168.50.100"
  config.vm.hostname = "vagrant.dev"
```

To mount and host a project site, place your docroot files in directory `/web`.
Then, edit `/Vagrantfile` and add the following:

```ruby
  # Synced folders.
  config.vm.synced_folder ".", "/var/www/example.dev", :nfs => true
```
- Be sure to install NFS support on your host machine.
    - In Ubuntu:

            sudo apt-get install nfs-kernel-server

    - In Windows:

            vagrant plugin install vagrant-winnfsd

To add ssh agent forwarding to the box, edit `/Vagrantfile` and add the
following:

```ruby
  # SSH forward agent.
  config.ssh.forward_agent  = true
```
- Be sure to allow SSH Agent Forwarding on your host machine. To do so, edit
  `~/.ssh/config` and add the following:

        Host 127.0.0.1 localhost
          ForwardAgent yes

To add more memory to the box, edit `Vagrantfile` and add the following:

```ruby
  # Configure box memory.
  config.vm.provider :virtualbox do |vm|
    vm.customize ["modifyvm", :id, "--memory", 2048]
  end
```

### Vagrant box usage and notes

All Vagrant boxes are pre-configured with the following parameters.

- Web server is configured for automatic virtual hosts.
    - Project site docroot(s) should be located in: `/var/www/example.dev/web`
- Database root user / password:
    - root / root
- Database is accessible from the host machine via port 3306.
- PHPInfo page is available on port 8000:
    - <http://vagrant.dev:8000>
- Adminer is available on port 8181:
    - <http://vagrant.dev:8181>
- MailHog is available on port 8025:
    - <http://vagrant.dev:8025>
- Additional custom service settings as defined in this repository's `build`
  directory.

### ToDo

- Increase the max upload size in php.ini from 2M to something bigger.
- Routine updates

## References & Thanks

- [How to Create a Vagrant Base Box from an Existing One - Scotch][1]
- [How To Install Linux, Apache, MySQL, PHP stack on Ubuntu 14.04 - DigitalOcean][2]
- [How To Install Drupal on an Ubuntu 14.04 Server with Apache - DigitalOcean][3]
- [installing MailHog on Linux virtual box to capture outgoing emails - Stack Overflow][4]
- [Set up Automatic Virtual Hosts with Nginx and Apache - SitePoint][5]

[1]: https://scotch.io/tutorials/how-to-create-a-vagrant-base-box-from-an-existing-one
[2]: https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-14-04 
[3]: https://www.digitalocean.com/community/tutorials/how-to-install-drupal-on-an-ubuntu-14-04-server-with-apache
[4]: http://stackoverflow.com/questions/39115015/installing-mailhog-on-linux-virtual-box-to-capture-outgoing-emails
[5]: https://www.sitepoint.com/set-automatic-virtual-hosts-nginx-apache
