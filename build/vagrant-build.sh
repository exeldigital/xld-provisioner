#!/bin/bash

# This script provisions a Vagrant box based on select arguments.

# Check that arguments have been passed.
USAGE="Usage: $0 [apache2,nginx] [php5,php7]"
if [ "$#" == "0" ]; then
  echo "$USAGE"
  exit 1
fi

# Process arguments passed to this script.
server=$1
php=$2
build_path=$(dirname "$0")
cd $build_path

# Validate server and php arguments.
if [ ! -e "$build_path/$server/build.sh" ]; then
  echo -e "\n=== $build_path/$server/build.sh not found. Exiting. ===\n"
  exit 1
fi
if [ ! -e "$build_path/$php/build.sh" ]; then
  echo -e "\n=== $build_path/$php/build.sh not found. Exiting. ===\n"
  exit 1
fi

# Run the build scripts.
echo -e "\n=== BASIC APTITUDE UPDATES ==="
source "$build_path/aptitude/build.sh"
echo -e "\n=== INSTALLING WEB SERVER ==="
source "$build_path/$server/build.sh"
echo -e "\n=== INSTALLING MYSQL ==="
source "$build_path/mysql/build.sh"
echo -e "\n=== INSTALLING PHP ==="
source "$build_path/$php/build.sh"
echo -e "\n=== INSTALLING DEVELOPMENT TOOLS ==="
source "$build_path/tools/build.sh"
echo -e "\n=== CONFIGURING ADDITONAL SETTINGS FOR VAGRANT ==="
source "$build_path/vagrant/build.sh"
echo -e "\n=== RESTARTING WEB SERVER & DATABASE ==="
if [ "$server" == "nginx" ]; then
  if [ "$php" == "php5" ]; then
    sudo service php5.6-fpm restart
  elif [ "$php" == "php7" ]; then
    sudo service php7.1-fpm restart
  fi
fi
sudo service $server restart && sudo service mysql restart
echo -e "\n=== CLEANING UP ==="
source "$build_path/cleanup/build.sh"
echo -e "\n=== ALL DONE ===\n"
