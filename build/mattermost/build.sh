#!/bin/bash

echo -e "\n=== Setting up Mattermost Server ===\n"
wget https://releases.mattermost.com/3.8.2/mattermost-3.8.2-linux-amd64.tar.gz -O mattermost.tar.gz
tar -xvzf mattermost.tar.gz
rm -v mattermost.tar.gz
sudo mv -v mattermost /opt
sudo mkdir -p /opt/mattermost/data
sudo useradd -r mattermost -U
sudo chown -R mattermost:mattermost /opt/mattermost
sudo chmod -R g+w /opt/mattermost
sudo usermod -aG mattermost ${USER}
cd /opt/mattermost/config

echo -e "\n=== Copying Mattermost config files ===\n"
local_path=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)
sudo cp -Rv $local_path/opt /
sudo cp -Rv $local_path/etc /
