#!/bin/bash

echo -e "\n=== Installing Nginx ===\n"
sudo apt-get -y install nginx

echo -e "\n=== Removing the default site ===\n"
sudo rm /etc/nginx/sites-enabled/default
sudo rm -rv /usr/share/nginx/html
