#!/bin/bash

echo -e "\n=== Installing PHP-Xdebug ===\n"
sudo apt-get -y install php-xdebug
sudo phpdismod xdebug

echo -e "\n=== Copying Vagrant config files ===\n"
local_path=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)
sudo cp -Rv $local_path/etc /
if [ "$server" == "apache2" ]; then
  sudo rm -rfv /etc/nginx
  sudo rm -rfv /etc/php/*/fpm
elif [ "$server" == "nginx" ]; then
  sudo rm -rfv /etc/apache2
  sudo rm -rfv /etc/php/*/apache2
fi
if [ "$php" == "php5" ]; then
  sudo rm -rfv /etc/php/7.*
elif [ "$php" == "php7" ]; then
  sudo rm -rfv /etc/php/5.*
fi

echo -e "\n=== Adding php info page on port 8000 ===\n"
mkdir -v /usr/local/src/phpinfo
echo "<?php phpinfo(); ?>" > /usr/local/src/phpinfo/index.php

echo -e "\n=== Installing MailHog on port 8025 ===\n"
mkdir -v /usr/local/src/mailhog && cd /usr/local/src/mailhog
wget https://github.com/mailhog/MailHog/releases/download/v0.2.1/MailHog_linux_amd64 \
  -O MailHog
chmod 775 MailHog
sudo ln -sv /usr/local/src/mailhog/MailHog /usr/local/bin/MailHog
wget https://github.com/mailhog/mhsendmail/releases/download/v0.2.0/mhsendmail_linux_amd64 \
  -O mhsendmail
chmod 775 mhsendmail
sudo ln -sv /usr/local/src/mailhog/mhsendmail /usr/local/bin/mhsendmail
sudo /etc/rc.local

echo -e "\n=== Installing Adminer on port 8181 ===\n"
mkdir -v /usr/local/src/adminer
wget "http://www.adminer.org/latest.php" -O /usr/local/src/adminer/index.php

echo -e "\n=== Configuring MySQL for root access from the host machine ===\n"
mysql -u"root" -p"$dbpass" -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY '$dbpass' WITH GRANT OPTION;"
mysql -u"root" -p"$dbpass" -e "FLUSH PRIVILEGES;"

echo -e "\n=== Creating '~/.my.cnf' ===\n"
echo -e "[client]\nuser     = root\npassword = root" > ~/.my.cnf
