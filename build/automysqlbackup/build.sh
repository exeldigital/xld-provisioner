#!/bin/bash

sudo apt-get install automysqlbackup

echo -e "\n=== Copying AutoMysqlBackup config files ===\n"
local_path=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)
sudo cp -Rv $local_path/etc /
