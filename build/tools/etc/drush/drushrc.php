<?php

/**
 * @file
 * Drush RC file.
 *
 * For more information, see:
 * https://github.com/drush-ops/drush/blob/master/examples/example.drushrc.php .
 */

// Execute 'git pull'.
$options['shell-aliases']['pull'] = '!git pull';

// Execute git pull and update.php.
$options['shell-aliases']['pulldb'] = '!git pull && drush updatedb';

// List site modules.
$options['shell-aliases']['noncore'] = 'pm-list --no-core';

// Place Drupal 8 site in maintenance mode.
$options['shell-aliases']['offline'] = 'state-set system.maintenance_mode 1 --input-format=integer';

// Place Drupal 7 site in maintenance mode.
$options['shell-aliases']['offline7'] = 'variable-set -y --always-set maintenance_mode 1';

// Take Drupal 8 site out of maintenance mode.
$options['shell-aliases']['online'] = 'state-set system.maintenance_mode 0 --input-format=integer';

// Take Drupal 7 site out of maintenance mode & clear caches.
$options['shell-aliases']['online7'] = '!drush variable-delete -y --exact maintenance_mode && drush cc all';

// Display site connection information.
$options['shell-aliases']['self-alias'] = 'site-alias @self --with-db --alias-name=new';

// Check git status.
$options['shell-aliases']['git-status'] = '!git status';

// Check the git branch.
$options['shell-aliases']['git-branch'] = '!git branch -a';

// List the 5 most recent git commits.
$options['shell-aliases']['git-log'] = '!git log -5';

// Show the log message and textual diff for the most recent git commit.
$options['shell-aliases']['git-show'] = '!git show';
