#!/bin/bash

# If $dbpass is empty, then assign a default db password of 'root'.
if [ -z "$dbpass" ]; then
  dbpass="root"
fi

echo -e "\n=== Installing MySQL 5.6 ===\n"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $dbpass"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $dbpass"
sudo apt-get -y install mysql-server-5.6

echo -e "\n=== Copying MySQL config files ===\n"
local_path=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)
sudo cp -Rv $local_path/etc /
