#!/bin/bash

echo -e "\n=== Installing Postfix ===\n"
debconf-set-selections <<< "postfix postfix/mailname string exeldigital.com"
debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'"
sudo DEBIAN_PRIORITY=low apt-get install -y postfix opendkim opendkim-tools

echo -e "\n=== Copying Postfix & OpenDKIM config files ===\n"
local_path=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)
sudo cp -Rv $local_path/etc /
sudo postmap /etc/postfix/virtual
sudo chown opendkim:opendkim /etc/opendkim/keys/*/mail.private

echo -e "\n=== Configuring firewall ===\n"
sudo ufw allow Postfix

echo -e "\n=== Restarting Postfix ===\n"
sudo service postfix restart
