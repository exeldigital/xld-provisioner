#!/bin/bash

echo -e "\n=== Installing PHP 7.1 ===\n"
if [ "$server" == "apache2" ]; then
  sudo apt-get -y install php7.1
elif [ "$server" == "nginx" ]; then
  sudo apt-get -y install php7.1-fpm
fi

echo -e "\n=== Installing PHP core extensions ===\n"
sudo apt-get -y install php7.1-curl \
                        php7.1-gd \
                        php7.1-mbstring \
                        php7.1-mcrypt \
                        php7.1-mysql \
                        php7.1-soap \
                        php7.1-xml \
                        php7.1-zip

echo -e "\n=== Copying PHP config files ===\n"
local_path=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)
sudo cp -Rv $local_path/etc /
if [ "$server" == "apache2" ]; then
  sudo rm -rfv /etc/php/*/fpm
fi
