#!/bin/bash

echo -e "\n=== Installing PHP 5.6 ===\n"
if [ "$server" == "apache2" ]; then
  sudo apt-get -y install php5.6
elif [ "$server" == "nginx" ]; then
  sudo apt-get -y install php5.6-fpm
fi

echo -e "\n=== Installing PHP core extensions ===\n"
sudo apt-get -y install php5.6-curl \
                        php5.6-gd \
                        php5.6-mbstring \
                        php5.6-mcrypt \
                        php5.6-mysql \
                        php5.6-soap \
                        php5.6-xml \
                        php5.6-zip

if [ "$server" == "apache2" ]; then
  echo -e "\n=== Installing php-uploadprogress ===\n"
  sudo apt-get -y install php-uploadprogress
fi

echo -e "\n=== Copying PHP config files ===\n"
local_path=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)
sudo cp -Rv $local_path/etc /
if [ "$server" == "apache2" ]; then
  sudo rm -rfv /etc/php/*/fpm
fi
