#!/bin/bash

echo -e "\n=== Copying Cloud config files ===\n"
local_path=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)
sudo cp -Rv $local_path/etc /
if [ "$server" == "apache2" ]; then
  sudo rm -rfv /etc/nginx
  sudo rm -rfv /etc/php/*/fpm
elif [ "$server" == "nginx" ]; then
  sudo rm -rfv /etc/apache2
fi
if [ "$php" == "php5" ]; then
  sudo rm -rfv /etc/php/7.*
elif [ "$php" == "php7" ]; then
  sudo rm -rfv /etc/php/5.*
fi

echo -e "\n=== Configuring server timezone ===\n"
if [ -n "$timezone" ]; then
  echo $timezone | sudo tee /etc/timezone
  sudo dpkg-reconfigure -f noninteractive tzdata
fi

echo -e "\n=== Installing php info page on port 8000 ===\n"
mkdir -v /usr/local/src/phpinfo
echo "<?php phpinfo(); ?>" > /usr/local/src/phpinfo/index.php

echo -e "\n=== Installing Adminer on port 8181 ===\n"
mkdir -v /usr/local/src/adminer
wget "http://www.adminer.org/latest.php" -O /usr/local/src/adminer/index.php

echo -e "\n=== Configuring Certbot SSL ===\n"

sudo add-apt-repository -y ppa:certbot/certbot
sudo apt-get update
sudo apt-get -y install python-certbot-apache 

certbot certonly

echo -e "\n=== Configuring Firewall ===\n"
sudo ufw allow OpenSSH
sudo ufw allow in "Apache Full"
sudo ufw enable
sudo ufw status

echo -e "\n=== Securing MySQL ===\n"
mysql -u"root" -p"$dbpass" -e "DELETE FROM mysql.user WHERE User='';"
mysql -u"root" -p"$dbpass" -e "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');"
mysql -u"root" -p"$dbpass" -e "DELETE FROM mysql.db WHERE db LIKE 'tes%' AND user='';"
mysql -u"root" -p"$dbpass" -e "FLUSH PRIVILEGES;"

echo -e "\n=== Restarting SSH ===\n"
sudo service sshd restart
